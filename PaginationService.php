<?php
/*PaginationService.php
Symfony service to help paginate doctrine array collections
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Services;

class PaginationService
{
    protected $Doctrine = null;
    protected $items_per_page = null;
    protected $total_pages = null;
    protected $collection_total = null;
    protected $offset = null;
   
    public function __construct($Doctrine)
    {  
        $this->Doctrine = $Doctrine;
        $this->setConfiguration();
    }

    public function setConfiguration()
    {
        $Repository = $this->Doctrine->getRepository('CYINTSettingsBundle:Setting');
        $pagination = $Repository->findByNamespace('pagination');
        $this->setItemsPerPage($pagination['items_per_page']);
    }

    public function setItemsPerPage($items_per_page)
    {
        $this->items_per_page = $items_per_page;
    }

    public function getItemsPerPage()
    {
        return $this->items_per_page;
    }

    public function initializeCollectionData($collection)
    {
        $length = $this->getItemsPerPage();
        $total = count($collection);
        $pages = ceil($total / $length);
        $this->setTotalPages($pages);
        $this->setCollectionTotal($total);
    }

    public function pageResults($collection, $page)
    {
        $length = $this->getItemsPerPage();
        $total = $this->getCollectionTotal();
        $pages = $this->getTotalPages();
       
        if($page > $pages)
            throw new PaginationServiceException('That page of results does not exist.');
                
        $offset = ( ($page-1)  * $this->getItemsPerPage() );
        $this->setOffset($offset);

        if( ($offset + $length) > $total )      
            $length = ($total - $offset);
              
        return $collection->slice($offset, $length);
    }

    public function getTotalPages() 
    {
        return $this->total_pages;
    }


    public function setTotalPages($total_pages)
    {
        $total_pages = empty($total_pages) ? 1 : $total_pages;
        $this->total_pages = $total_pages;
    }

    public function getCollectionTotal()
    {
        return $this->collection_total;
    }

    public function setCollectionTotal($collection_total)
    {
        $this->collection_total = $collection_total;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function processPagination($collection, $page)
    {
        $this->initializeCollectionData($collection);
        $subset = $this->pageResults($collection, $page);
        $pages = $this->getTotalPages();
        $total = $this->getCollectionTotal();
        $offset = $this->getOffset();

        return new Pagination($subset, $pages, $total, $offset, $page);
    }

}


class PaginationServiceException extends \Exception
{
    public function __construct($message) 
    {
        parent::__construct($message);
    }
}


?>
